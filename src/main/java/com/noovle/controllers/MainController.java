package com.noovle.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping("/print")
    public String print() {
        return "Hello ";
    }

    // @RequestParam(name = "name", defaultValue = "My friend") String name
}
