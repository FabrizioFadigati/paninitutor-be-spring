package com.noovle.paninitutorbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.noovle.controllers")
public class PaninitutorbeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaninitutorbeApplication.class, args);
    }

}
