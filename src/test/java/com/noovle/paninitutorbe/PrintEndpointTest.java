package com.noovle.paninitutorbe;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

// Spring Boot Test annotation is used to scan the @SpringBootApplication annotations
// in order to find the application context
// @SpringBootTest

// Auto config Mock Mvc - it is a bean or component
@AutoConfigureMockMvc
// Instancing only the selected components we desire
@WebMvcTest
public class PrintEndpointTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void printTest() {

        System.out.println("Test print");
        // test
    }
}
